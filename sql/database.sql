-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generato il: Ago 12, 2013 alle 17:37
-- Versione del server: 5.5.25
-- Versione PHP: 5.4.4

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `gioco`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `giocatore`
--

CREATE TABLE `giocatore` (
  `ID_giocatore` int(11) NOT NULL AUTO_INCREMENT,
  `NOME_giocatore` varchar(255) NOT NULL,
  `EMAIL_giocatore` varchar(255) NOT NULL,
  `PASSWORD_giocatore` varchar(255) NOT NULL,
  `PRIVILEGI_giocatore` int(11) NOT NULL,
  PRIMARY KEY (`ID_giocatore`),
  UNIQUE KEY `EMAIL_giocatore` (`EMAIL_giocatore`),
  UNIQUE KEY `NOME_giocatore` (`NOME_giocatore`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dump dei dati per la tabella `giocatore`
--

INSERT INTO `giocatore` (`ID_giocatore`, `NOME_giocatore`, `EMAIL_giocatore`, `PASSWORD_giocatore`, `PRIVILEGI_giocatore`) VALUES
(1, 'g1', 'g1@de.it', 'giocatore1', 0),
(2, 'g2', 'g2@de.it', 'giocatore2', 0),
(3, 'g3', 'g3@de.it', 'giocatore3', 0),
(4, 'g4', 'g4@de.it', 'giocatore4', 0),
(5, 'g5', 'g5@de.it', 'giocatore5', 0),
(7, 'Marco', 'marco@de.it', 'marcopass', 0),
(8, 'Marco2', 'marco2@de.it', 'marco2pass', 0),
(9, 'marco3', 'marco3@de.it', 'marco3pass', 0),
(12, 'marco4', 'marco4@de.it', 'marco4pass', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `giocatori_partita`
--

CREATE TABLE `giocatori_partita` (
  `id_partita` int(11) NOT NULL,
  `id_giocatore` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `partita`
--

CREATE TABLE `partita` (
  `ID_partita` int(11) NOT NULL AUTO_INCREMENT,
  `NOME_partita` varchar(255) NOT NULL,
  `id_giocatore` int(11) NOT NULL,
  PRIMARY KEY (`ID_partita`),
  UNIQUE KEY `Nome` (`NOME_partita`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dump dei dati per la tabella `partita`
--

INSERT INTO `partita` (`ID_partita`, `NOME_partita`, `id_giocatore`) VALUES
(2, 'partita', 2),
(3, 'partita 2', 3),
(4, 'partita 3', 4),
(5, 'partita mia', 4),
(6, 'nuova partita', 4),
(7, 'partita del giocatore 3', 3),
(8, 'oirnfe', 3),
(9, 'partita del giocatore 2', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `turno_attesa`
--

CREATE TABLE `turno_attesa` (
  `IDgiocatore` int(11) NOT NULL,
  `IDpartita` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `turno_inizio`
--

CREATE TABLE `turno_inizio` (
  `IDpartita` int(11) NOT NULL,
  `numero_turno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
